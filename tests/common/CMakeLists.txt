set(TARGET lomiri-download-manager-test-lib)

set(SOURCES
        base_testcase.cpp
        daemon_testcase.cpp
        local_tree_testcase.cpp
        testing_interface.cpp
)

set(HEADERS
        apparmor.h
        base_testcase.h
        daemon_testcase.h
        dbus_connection.h
        file_manager.h
        local_tree_testcase.h
        matchers.h
        network_reply.h
        process.h
        process_factory.h
        request_factory.h
        testing_interface.h
        uuid_factory.h
)

include_directories(${DBUS_INCLUDE_DIRS})
include_directories(${GTEST_INCLUDE_DIRS})
include_directories(${GMOCK_INCLUDE_DIRS})
include_directories(${CMAKE_CURRENT_SOURCE_DIR})
include_directories(${CMAKE_CURRENT_BINARY_DIR})
include_directories(${CMAKE_SOURCE_DIR}/src/common/public)
include_directories(${CMAKE_SOURCE_DIR}/src/common/priv)
include_directories(${CMAKE_SOURCE_DIR}/src/downloads/common)
include_directories(${CMAKE_SOURCE_DIR}/src/downloads/client)
include_directories(${CMAKE_SOURCE_DIR}/src/downloads/priv)

add_library(${TARGET} STATIC
    ${HEADERS}
    ${SOURCES}
)

target_link_libraries(${TARGET}
    Qt5::Core
    Qt5::Sql
    Qt5::DBus
    Qt5::Test
    ${GMOCK_LIBRARIES}
    ${GTEST_BOTH_LIBRARIES}
    ldm-common
    ldm-priv-common
    lomiri-download-manager-common
    lomiri-download-manager-client
)
